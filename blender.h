#ifndef GM_PARAMETRICS_CURVES_BLENDER_H
#define GM_PARAMETRICS_CURVES_BLENDER_H

#include <parametrics/gmpcurve.h>
#include "helperFunctions.h"

namespace GMlib
{

  template <typename T>
  class Blender : public PCurve<T,3>
  {
    GM_SCENEOBJECT(Blender)

    public:
      Blender(PCurve<float,3>* c1, PCurve<float,3>* c2, T t);
    void checkForUpdates();


  protected:
    //Virtual protected functions from PCurve
    void            eval(T t, int d, bool l)const override;
    T               getStartP() const override;
    T               getEndP() const override;

    // Simulation
    void localSimulate(double dt) override;

    //Protected Intrinsic data for blender i.e. the curves to blend
    PCurve<float,3>*    _curve1;
    PCurve<float,3>*    _curve2;
    T                   _blendOverlap;

  };   //END class Blender


  //*****************************
  //Default Constructor
  //*****************************
  template <typename T>
  inline
  Blender<T>::Blender(PCurve<float,3>* c1, PCurve<float,3>* c2, T t)
  {
    _curve1 = c1;
    _curve2 = c2;
    _blendOverlap = t;

    _curve2->setDomain(_curve1->getParStart() + (1 - _blendOverlap) * _curve1->getParDelta(),//s1+0.8*delta1
                       _curve1->getParStart() + (2 - _blendOverlap) * _curve1->getParDelta());
    //parDelta is equal to (end - start)
    //domain is also scaled
  }


  //****************************************
  //Implementation of the formula for the blender
  //****************************************
  template<typename T>
  void Blender<T>::eval(T t, int d, bool /*l*/) const
  {

    this->_p.setDim( d + 1 );

    auto domain_2 = _curve1->getParEnd();
    auto domain_1 = domain_2 - _blendOverlap * _curve1->getParDelta();

    if(t<=domain_1)
      this->_p[0] = _curve1->evaluateParent(t,0)[0];

    if(domain_1<t && t<domain_2){
      T b =  Utilities::BFunctions::polynomialFunction((t - _curve2->getParStart())/(_curve1->getParEnd() - _curve2->getParStart()));
      Vector<T,3> p1 = _curve1->evaluateParent(t,0)[0];
      Vector<T,3> p2 = _curve2->evaluateParent(t,0)[0];
      this->_p[0] = (p1 + b * (p2 - p1)) ;
    }

    if(domain_2<=t)
      this->_p[0] = _curve2->evaluateParent(t,0)[0];
  }


  //****************************************************
  //The start and end of the defined domain (interval)
  //****************************************************
  template<typename T>
  T Blender<T>::getStartP() const {
    return _curve1->getParStart();
  }


  template<typename T>
  T Blender<T>::getEndP() const {
    return _curve2->getParEnd();
  }


  //****************************************************
  //Object Functions
  //****************************************************
  template<typename T>
  void Blender<T>::checkForUpdates(){
    if (_curve1->getEditDone() || _curve2->getEditDone())
    {
      this->sample(100,4);
      this->replot();
    }
  }

  template<typename T>
  void Blender<T>::localSimulate(double /*dt*/)
  {
    this->sample(100, 0);

  }

#endif //GM_PARAMETRICS_CURVES_BLENDER_H
}// END namespace GMlib
