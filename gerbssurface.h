#ifndef GM_PARAMETRICS_CURVES_GERBS_SURF_H
#define GM_PARAMETRICS_CURVES_GERBS_SURF_H

#include <random>
#include <memory>
// gmlib
#include "simplesubsurf.h"

namespace GMlib{

    enum class SurfaceShape{
        None,
        Plane,
        Cylinder,
        Torus
    };

    template <typename T>
    class GERBSSurf : public PSurf<T,3> {
        GM_SCENEOBJECT(GERBSSurf)
public:
    GERBSSurf( GMlib::PSurf<T, 3>* surf, int n1, int n2, SurfaceShape _shape);

    //Virtual public function from PCurve
    bool                        isClosedU() const override;
    bool                        isClosedV() const override;

protected:
    //Virtual protected functions from PCurve
    void                        eval(T u, T v, int d1=0, int d2=0, bool lu=false, bool lv=false) const;
    T                           getStartPU() const override;
    T                           getStartPV() const override;
    T                           getEndPU() const override;
    T                           getEndPV() const override;
    T                           getModelStartPU() const;
    T                           getModelStartPV() const;
    T                           getModelEndPU() const;
    T                           getModelEndPV() const;
    void                        localSimulate(double dt) override;

private:
    void                        generateKnots(std::vector<T>& t, int n, int k, T start, T end, bool close);
    void                        prepareLocalSurfaces();
    void                        createSubSurf(int u, int v);
    Vector<T,2>                 trigonometricFunction(T t)  const;
    T                           findI(T t, const std::vector<T>& _t, int d, int n) const;
    T                           getW(T t, const std::vector<T>& _t, int d, int i) const;
    void                        reShape();

    //Intrinsic data for curve
    DMatrix< Vector<T,3>>       _c;             // Control polygon
    T                           _du;            // Degree
    T                           _dv;            // Degree
    T                           _nu;            // Number of control points in 1st dir
    T                           _nv;            // Number of control points in 2nd dir
    T                           _k=2;           // Number of order
    std::vector<T>              _u;             // Knot Vector of 1st dir
    std::vector<T>              _v;             // Knot Vector of 2nd dir
    SurfaceShape                _shape;
    // Container for curves
    PSurf<T,3>*                 _surf;          // Model curve
    DMatrix<PSurf<T,3>*>        _local_surf;    // Container of Sub-curves

    std::uniform_real_distribution<double>  _dis;
    std::mt19937                            _gen;



};   //End class GERBSSurf

//Include GERBS class function implementation

//*****************************
//Default constructor
//*****************************
template <typename T>
inline
GERBSSurf<T>::GERBSSurf( GMlib::PSurf<T, 3>* model, int n1, int n2, SurfaceShape shape){

    GMlib::Parametrics<T,2,3>::setDerivationMethod(GMlib::GM_DERIVATION_METHOD::GM_DERIVATION_DD);

    _surf = model;
    _nu = n1;
    _nv = n2;
    _du = 1;
    _dv = 1;
    _shape = shape;

    if(isClosedU())
        _nu++;
    if(isClosedV())
        _nv++;

    _local_surf.setDim(_nu,_nv);

    generateKnots(_u,_nu,_k,getModelStartPU(),getModelEndPU(),isClosedU());
    generateKnots(_v,_nv,_k,getModelStartPV(),getModelEndPV(),isClosedV());
    prepareLocalSurfaces();

    if(_shape != SurfaceShape::None)
        reShape();
}

//*****************************
//This curve is cyclical
//*****************************
template <typename T>
bool  GERBSSurf<T>::isClosedU() const {return _surf->isClosedU();}

template <typename T>
bool  GERBSSurf<T>::isClosedV() const {return _surf->isClosedV();}

//****************************************
//Implementation of formula for the curve
//****************************************
template<typename T>
void GERBSSurf<T>::eval(T u, T v, int du , int dv, bool /*lu*/, bool /*lv*/) const {

    this->_p.setDim(du+1,dv+1);

    auto iu = findI(u,_u,du,_nu);
    auto iv = findI(v,_v,dv,_nv);
    auto bu = trigonometricFunction(getW(u,_u,du,iu));
    auto bv = trigonometricFunction(getW(v,_v,dv,iv));

    DMatrix<Vector<T,3>> p00 = _local_surf(iu-1)(iv-1)->evaluateParent(u,v,du,dv);
    DMatrix<Vector<T,3>> p01 = _local_surf(iu)(iv-1)->evaluateParent(u,v,du,dv);
    DMatrix<Vector<T,3>> p10 = _local_surf(iu-1)(iv)->evaluateParent(u,v,du,dv);
    DMatrix<Vector<T,3>> p11 = _local_surf(iu)(iv)->evaluateParent(u,v,du,dv);

    DMatrix<Vector<T,3>> s1 = ((1-bu[0]) * p00) + (bu[0] * p01);
    DMatrix<Vector<T,3>> s2 = ((1-bu[0]) * p10) + (bu[0] * p11);

    auto c      = ((1-bv[0]) * s1) + (bv[0] * s2);
    this->_p    = c;
}

//****************************************
//Local simulate function
//****************************************
template<typename T>
void GERBSSurf<T>::localSimulate(double /*dt*/) {this->sample(25, 25, 1, 1);}

//****************************************
//Functions
//****************************************
template<typename T>
Vector<T,2> GERBSSurf<T>::trigonometricFunction(T t) const{
    //***********************
    //* B(t)= sin^2(PI/2*t) *
    //***********************
    Vector<T,2> s;
    s[0] = sin((M_PI/2)*t);
    s[0]*= s[0];
    s[1] = M_PI*sin((M_PI/2)*t)*cos((M_PI/2)*t);
    return s;
}

template<typename T>
T GERBSSurf<T>::getW(T t, const std::vector<T>& _t, int d, int i) const {
    return ((t - _t[i])/( _t[i+d] - _t[i]));
}

template<typename T>
T GERBSSurf<T>::findI(T t, const std::vector<T>& _t, int d, int n) const {

    int i=d;
    for (; i < n; i++ )
      if (_t[i] <= t && t < _t[i+1])
        break;

    if (i >= n)
      i = n-1;
    return i;
}

template<typename T>
void GERBSSurf<T>::generateKnots(std::vector<T>& t, int n, int k, T start, T end, bool close) {

    t.resize(n+k);

    t[0] = start;
    t[1] = t[0];
    T dt = (end - start)/T(n-1);

    for (int i =2 ;i<n;i++) {
        t[i] = t[1] + (T(i-1) * dt);
    }
    t[n] = end;
    t[n+1] = t[n];

    if(close){  // Re-calculate the first and last knot
        t[0]    = t[1] - (t[n] - t[n-1]);
        t[n+1]  = t[n] + (t[2] - t[1]);
    }
}

template<typename T>
void GERBSSurf<T>::prepareLocalSurfaces(){
    int u, v;

    // Fill in all the rows except last one
    for (u = 0; u < _nu-1; u++)
      // Fill in all columns except last one
      for (v = 0; v < _nv-1; v++)
            createSubSurf(u, v);



    for (u = 0; u < _nu-1; u++){
        if (isClosedV())
            // If closed on v then subsurf in last column
            _local_surf[u][_nv-1] = _local_surf[u][0];
        else
            // If not then make new subsurface
            createSubSurf(u, _nv-1);
      }


    // Fill in last row
    for (v = 0; v < _nv-1; v++) {
      if (isClosedU())
            // If closed on u then subsurf in last row
            _local_surf[_nu-1][v] = _local_surf[0][v];
      else
            // If not then make new subsurface
            createSubSurf(_nu-1, v);
    }
    // Check for last element in Matrix
    if(isClosedV())
            _local_surf[_nu-1][_nv-1] = _local_surf[_nu-1][0];

    else if(isClosedU())
            _local_surf[_nu-1][_nv-1] = _local_surf[0][_nv-1];

    else
            createSubSurf(_nu-1,_nv-1);
}

template<typename T>
void GERBSSurf<T>::createSubSurf(int u, int v){

    _local_surf[u][v] = new PSimpleSubSurf<T>(
        _surf,
        _u[u], _u[u + 2], _u[u + 1],
        _v[v], _v[v + 2], _v[v + 1]);

    _local_surf[u][v]->toggleDefaultVisualizer();
    _local_surf[u][v]->setCollapsed(true);
    _local_surf[u][v]->sample(10, 10, 1, 1);
    this->insert(_local_surf[u][v]);
}

template<typename T>
void GERBSSurf<T>::reShape(){

    if(_shape == SurfaceShape::Plane){
        for (auto i = 1; i < _nu; ++i) {
            for (int j = 1; j < _nv-1; j++) {
                if (i == _nu-1) continue;
                auto trans1 = float(3.5*cos(i)*sin(j*M_PI/2));
                auto trans2 = float(1.5*_dis(_gen));
                _local_surf[i][j]->translateParent({ trans1 < 1 ? trans2 : trans1, 0.0f, 0.0f});
            }
        }
    }

    if(_shape == SurfaceShape::Cylinder){
        float trans = 0.3f;
        for (int j = 0; j < _nv; j++){
            for( int i = 0; i< _nu; i++){
                _local_surf[i][j]->translateParent({ 0.0f, i % 2 ? trans : -trans, 0.0f});
            }
        }
    }

    if(_shape == SurfaceShape::Torus){
        float trans = 0.5f;
        for (auto j = 0; j < _nv; j++) {
            for (int i = 0; i < _nu; i++) {
                if (i == _nu-1 or i == _nv-1) continue;
                _local_surf[i][j]->translateParent({ 0.0f, 0.0f, i % 2 ? trans : -trans });
            }
        }
    }
}
//****************************************************
//The start and end of the defined domain (intervall)
//****************************************************

template<typename T>
T GERBSSurf<T>::getStartPU() const { return _u[_du];}

template<typename T>
T GERBSSurf<T>::getEndPU() const { return _u[_u.size()-_k];}

template<typename T>
T GERBSSurf<T>::getStartPV() const { return _v[_dv];}

template<typename T>
T GERBSSurf<T>::getEndPV() const { return _v[_v.size()-_k];}

template<typename T>
T GERBSSurf<T>::getModelStartPU() const { return _surf->getParStartU();}

template<typename T>
T GERBSSurf<T>::getModelEndPU() const { return _surf->getParEndU();}

template<typename T>
T GERBSSurf<T>::getModelStartPV() const { return _surf->getParStartV();}

template<typename T>
T GERBSSurf<T>::getModelEndPV() const { return _surf->getParEndV();}

#endif //GM_PARAMETRICS_CURVES_GERBS_H
}
