#ifndef GM_PARAMETRICS_CURVES_GERBS_H
#define GM_PARAMETRICS_CURVES_GERBS_H

#include <parametrics/gmpcurve.h>
#include <parametrics/curves/gmpsubcurve.h>

#include "helperFunctions.h"

  namespace GMlib{

  template <typename T>
  class GERBS : public PCurve<T,3> {
    GM_SCENEOBJECT(GERBS)

    public:
      GERBS(PCurve<T,3>* curve, int n, bool closed = false);
      GERBS(const GERBS<T>& copy);

    protected:
      //Virtual protected functions from PCurve
      void            eval(T t, int d, bool l)const override;
      T               getStartP() const override;
      T               getEndP() const override;

      // Simulation
      void localSimulate(double dt) override;

      // Protected Intrinsic data for the curve
      DVector<Vector<T,3>>    _point;         //!< Points on the curve
      DVector<T>              _t;             //!< Knot Vector
      int                     _d;             //!< Polynomial Degree
      int                     _k;             //!< Order of Gerbs
      int                     _m, _n;
      bool                    _cl = false;

      // Animation
      double x = 0, y = 0, timer = 0;
      bool expand = true;
      GMlib::Color _color;


      // Curves
      PCurve<T,3>*            _curve;         // Model curve
      DVector<PCurve<T,3>*>   _local_curve;   // Container of Sub-curves


  private:
      // Local helper functions
      void            prepareLocalCurves();

  };   //End class GERBS


  //*****************************
  //Default Constructor
  //*****************************
  template <typename T>
  inline
  GERBS<T>::GERBS(PCurve<T,3>* curve, int n, bool closed) : PCurve<T,3>(20,0,0)
  {
//    _c =
    _d = 1;
    _curve = curve;
    _n = n;
    _k = _d+1;

    if (closed){
      _n++;
      _cl = closed;
    }



    Utilities::generateGerbKnotVector(_t, curve, _n, _k, _cl);
//    std::cout << _t << std::endl;
    prepareLocalCurves();
  }

  //*****************************
  //Copy constructor
  //*****************************
  template <typename T>
  inline
  GERBS<T>::GERBS(const GERBS<T>& copy):PCurve<T,3>(copy)
  {}


  //****************************************
  //Implementation of formula for the curve
  //****************************************
  template<typename T>
  void GERBS<T>::eval(T t, int d, bool /*l*/) const
  {
    this->_p.setDim(d+1);

    ///FORMULA: C(t)= (1-B(w_1,i(t)) * C_i-1(t)) + (B(w_1,i(t)) * C_i(t))
    int i = Utilities::findI(t,_t,_d,_n);
    auto b = Utilities::BFunctions::polynomialFunction(Utilities::calculateW(t,_t,1,i));
    Vector<T,3> p1 = _local_curve[i-1]->evaluateParent(t,0)[0];
    Vector<T,3> p2 = _local_curve[i]->evaluateParent(t,0)[0];

    this->_p[0] = ((1-b) * p1) + (b * p2);
  }


  //****************************************************
  // Start and end of the defined domain (intervall)
  //****************************************************
  template<typename T>
  T GERBS<T>::getStartP() const
  {
    return _t[_d];
  }

  template<typename T>
  T GERBS<T>::getEndP() const
  {
    return _t[_n];
  }


  //*****************************
  //Local Helper Functions
  //*****************************
  template<typename T>
  void GERBS<T>::prepareLocalCurves()
  {
    _local_curve.setDim(_n);
    for (int i =0; i < _n; i++)
    {
      if (_cl && i==_n-1)
        _local_curve[_n-1] = _local_curve[0];

      else{
        _local_curve[i] = new GMlib::PSubCurve<T>(_curve, _t[i], _t[i+_k], _t[i+_d]);
        _local_curve[i]->toggleDefaultVisualizer();
        _local_curve[i]->sample(20, 0);
        _local_curve[i]->setVisible(true);
        _local_curve[i]->setCollapsed(true);
        this->insert(_local_curve[i]);
      }
    }


  }

  template<typename T>
  void GERBS<T>::localSimulate(double dt)
  {
    GMlib::Vector<float,3> vec(sin(dt),cos(dt),5*dt);

    if (expand) {
      Utilities::Animations::expandCurve(dt, x, y);
      _color = GMlib::Color(20,200,100);
      _color.toHSV();
      this->setColor(_color);
    }
    else {
      Utilities::Animations::expandCurve(dt, x, y);
      _color = GMlib::Color(200,20,100);
      _color.toHSV();
      this->setColor(_color);
    }

    timer += dt;
    std::cout << "timer: " << timer << "expand: " << expand << std::endl;


    if (timer >= 1.0) {
      this->setColor(GMlib::GMcolor::gold());
      if (expand)
        expand = false;
      else
        expand = true;
      timer = 0.0;
    }

    for (int i =0; i < _local_curve.getDim()-1; i++){
      if (i % 2 == 0){
        _local_curve[i]->rotate(dt, vec);
        _local_curve[i]->translateGlobal(sin(y*10)*Vector<float,3>(vec));


      }
      else{
        _local_curve[i]->rotate(dt, -vec);
        _local_curve[i]->translateGlobal(sin(x*10)*Vector<float,3>(vec));

      }
    }

    this->sample(2000, 0);
  }

#endif //GM_PARAMETRICS_CURVES_GERBS_H
}
