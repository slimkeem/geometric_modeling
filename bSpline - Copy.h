#ifndef GM_PARAMETRICS_CURVES_BSPLINE_H
#define GM_PARAMETRICS_CURVES_BSPLINE_H


#include "parametrics/gmpcurve.h"

namespace GMlib {

  template <typename T>
  class BSpline : public PCurve<T,3> {
    GM_SCENEOBJECT(BSpline)

    public:
      BSpline(const DVector<Vector<T,3>>& c); //first constructor
      BSpline(const DVector<Vector<T,3>> p, int n); //2nd constructor

      // Debuggers/Getters
      const DVector<T>&           getKnotVector() const;


    protected:
      // Virtual protected functions from PCurve
      void          eval(T t, int d, bool l) const override;
      T             getStartP() const override;
      T             getEndP()   const override;

      // Protected intrinsic data for the curve
      DMatrix<T>                   _A;        //!< A Matrix
      DVector<Vector<T,3>>         _c;        //!< control points (control polygon)
      DVector<Vector<T,3>>         _point;
      DVector<T>                   _t;        //!< knot vector
      int                          _d;        //!< polynomial degree
      int                          _k;        //!< order of B-spline (_k = _d + 1)
      int                          _m, _n;


    private:
      // Local help functions
      void        generateKnotVector();
      int         findI(T t) const;
      float       calculateW(T t, int d, int i) const;
      Vector<T,3> generateBasis(T t, int i) const;
      T           calculateDt() const;
      void        generateAMatrix();
      void        generateControlPoints();

  }; // END class BSPline


  //*****************************************
  // Default Constructors
  //*****************************************
  template <typename T>
  inline
  BSpline<T>::BSpline(const DVector<Vector<T, 3>> &c) : PCurve<T,3>(20,0,2)
  {
    _d            = 2;
    _c            = c;
    _k            = _d+1;
    _n            = _c.getDim();

    generateKnotVector();
  }

  template <typename T>
  inline
  BSpline<T>::BSpline(const DVector<Vector<T,3>> p, int n) : PCurve<T,3>(20,0,2)
  {
    _d = 2;
    _n = n;
    _point = p;
    _k = _d+1;
    _m = p.getDim();

    generateKnotVector();
    generateAMatrix();
    generateControlPoints();
  }


  //*****************************************
  // Implementation of the formula for the curve
  //*****************************************
  template <typename T>
  void BSpline<T>::eval( T t, int d, bool /*1*/) const
  {

    this->_p.setDim( d + 1 );

    int i = findI(t);
    std::cout << t << " i " << i << std::endl;
    Vector<T, 3> b = generateBasis(t, i);

    this->_p[0] = b[0]*_c[i-2] + b[1]*_c[i-1] + b[2]*_c[i];

  }


  //*****************************************
  // Start and end of the defined domain (interval)
  //*****************************************
  template <typename T>
  T BSpline<T>::getStartP() const
  {
    return _t[2];
  }

  template <typename T>
  T BSpline<T>::getEndP() const
  {
    return _t[_c.getDim()];
  }


  //*****************************************
  // Helper Classes
  //*****************************************
  template <typename T>
  void BSpline<T>::generateKnotVector( /*int dim*/ )
  {
    int dim = _n + _k;
    _t.setDim(dim);
    int step = dim - (2 * _k);
    int i = 0;
    for( ; i < _k; i++ )              // Set the start knots
      _t[i] = T(0);
    for( int j = 1; j <= step; j++ )  // Set the "step"-knots
      _t[i++] = T(j);
    for( ; i < dim; i++ )             // Set the end knots
      _t[i] = T(step+1);
  }

  template<typename T>
  int BSpline<T>::findI(T t) const
  {
    for (int i = _d; i < _c.getDim(); ++i) {
      if (_t[i] <= t && t < _t[i+1])
        return i;
    }
    return _c.getDim()-1;
  }

  template<typename T>
  float BSpline<T>::calculateW(T t, int d, int i) const
  {
    return (t - _t[i]) / (_t[i+d] - _t[i]);
  }

  template<typename T>
  Vector<T,3> BSpline<T>::generateBasis(T t, int i) const
  {
    float w1_i = calculateW(t,1,i);
    float w2_iMinus = calculateW(t,2,i-1);
    float w2_i = calculateW(t,2,i);

    Vector<T, 3> b;
    DMatrix<T> A(1,2);
    DMatrix<T> B(2,3);

    A[0][0] = 1-w1_i;
    A[0][1] = w1_i;

    B[0][0] = 1-w2_iMinus;
    B[0][1] = w2_iMinus;
    B[0][2] = 0;

    B[1][0] = 0;
    B[1][1] = 1-w2_i;
    B[1][2] = w2_i;

    b[0] = A[0][0]*B[0][0];
    b[1] = A[0][0]*B[0][1] + A[0][1]*B[1][1];
    b[2] = A[0][1]*B[1][2];
    return b;
  }

  template<typename T>
  T BSpline<T>::calculateDt() const
  {
      return (_t[_n] - _t[_d])/(_m - 1);
  }

  template<typename T>
  void BSpline<T>::generateAMatrix()
  {
    _A = GMlib::DMatrix<T>(_m, _n, T(0));

    const T dt = this->calculateDt();
    GMlib::Vector<T,3> basis;
    for(int k =0; k < _m; k++)
    {
      auto t = _t[_d] + k*dt;
      int i = findI(t);
      basis = generateBasis(t, i);
      for (int l = i-_d; l <= i; l++)
      {
        _A[k][l] = basis[l-i+_d];
      }
    }
  }

  template<typename T>
  void BSpline<T>::generateControlPoints()
  {
    //!< Ax=b -> x=((A^t*A)^-1) *  (A^t*b)
    auto A_transpose = _A.transpose();
    auto AtA = A_transpose * _A;
    auto b = A_transpose * _point;
    _c = AtA.invert()*b;
  }


  //*****************************************
  // Debuggers/Getters
  //*****************************************
  template<typename T>
  const DVector<T> &BSpline<T>::getKnotVector() const
  {
    std::cout << "Knot Vector" << _t << std::endl;
    return _t;
  }


#endif // GM_PARAMETRICS_CURVES_BSpline
} // END namespace GMlib
