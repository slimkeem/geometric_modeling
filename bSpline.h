#ifndef GM_PARAMETRICS_CURVES_BSPLINE_H
#define GM_PARAMETRICS_CURVES_BSPLINE_H


#include "parametrics/gmpcurve.h"

#include "helperFunctions.h"

#include <scene/selector/gmselector.h>
#include <scene/visualizers/gmselectorgridvisualizer.h>
#include <scene/visualizers/gmselectorvisualizer.h>


namespace GMlib {

  template <typename T>
  class BSpline : public PCurve<T,3> {
    GM_SCENEOBJECT(BSpline)

    public:
      // Constructors
      BSpline(const DVector<Vector<T,3>>& c, bool closed = false); //first constructor
      BSpline(const DVector<Vector<T,3>> p, int n, bool closed = false); //2nd constructor

      // Virtual PUBLIC functions from SceneObject
      void            edit(int selector, const Vector<T,3>& dp) override;
      void            replot() const override;
      void            toggleSelectors();

      // Virtual PUBLIC functions from PCurve
      void            showSelectors( T radius = T(0.3), bool grid = false,
                                     const Color& selector_color = GMcolor::darkBlue(),
                                     const Color& grid_color = GMcolor::lightGreen() ) override;
      void            hideSelectors();

    protected:
      // Virtual protected functions from PCurve
      void          eval(T t, int d, bool l) const override;
      T             getStartP() const override;
      T             getEndP()   const override;

      // Simulation
      void localSimulate(double dt) override;

      // Protected intrinsic data for the curve
      DMatrix<T>                   _A;        //!< A Matrix for Least Square
      DVector<Vector<T,3>>         _c;        //!< control points (control polygon)
      DVector<Vector<T,3>>         _point;    //!< Points on the curve
      DVector<T>                   _t;        //!< knot vector
      int                          _d;        //!< polynomial degree
      int                          _k;        //!< order of B-spline (_k = _d + 1)
      int                          _m, _n, _selPoints;
      bool                         _cl = false;       //!< closed (or open) curve?

      mutable bool                 _c_moved;    //!< Mark that we are editing, moving controll points

      // Selectors and selector grid
      bool                         _selectors =false;        //!< Mark if we have selectors or not
      bool                         _grid = false;             //!< Mark if we have a selector grid or not
      SelectorGridVisualizer<T>*   _sgv;              //!< Selectorgrid
      std::vector<Selector<T,3>*>  _s;                //!< A set of selectors (spheres)
      T                            _selector_radius;
      Color                        _selector_color;
      Color                        _grid_color;

      bool                         _updateSampleFlag; //!< Mark if we want to move a selector

  }; // END class BSPline


  //*****************************************
  // Default Constructors
  //*****************************************
  template <typename T>
  inline
  BSpline<T>::BSpline(const DVector<Vector<T, 3>> &c, bool closed) : PCurve<T,3>(20,0,2)
  {
//    closed        = false;
    _d            = 2;
    _c            = c;
    _k            = _d+1;
    _n            = _c.getDim();
    _selPoints    = _n;

    if(_n <= _d)
      closed = true;

    _cl = closed;

    Utilities::generateKnotVector(_t, _n, _k, _cl);
  }

  template <typename T>
  inline
  BSpline<T>::BSpline(const DVector<Vector<T,3>> p, int n, bool closed) : PCurve<T,3>(20,0,2)
  {
    _d = 2;
    _n = n;
    _point = p;
    _selPoints = _point.getDim();
    _k = _d+1;
    _m = p.getDim();

    if(_n <= _d)
      closed = true;

    _cl = closed;

    Utilities::generateKnotVector(_t, _n, _k, true);
    Utilities::generateControlPoints(_A,_t,_c,_point,_d,_n,_m);
  }


  //****************************************
  //****** Virtual public functions   ******
  //****************************************
  template <typename T>
  void BSpline<T>::edit( int /*selector_id*/, const Vector<T,3>& /*dp*/ )
  {
    _c_moved = true;
    if( this->_parent ) this->_parent->edit( this );
    if( this->_derived ) this->_derived->edit( this );
    //       _pos_change.push_back(EditSet(selector_id, dp));
    _updateSampleFlag = true;
    this->setEditDone();
    _c_moved = false;
  }

  template <typename T>
  void BSpline<T>::replot() const
  {
    PCurve<T,3>::replot();
  }

  template <typename T>
  void BSpline<T>::showSelectors( T rad, bool grid, const Color& selector_color, const Color& grid_color )
  {
    if( !_selectors ) {

//      _s.resize(_selPoints);
      _s.resize(_c.getDim());

      for( int i = 0; i < _c.getDim(); i++ )
        if(this->isScaled())
          this->insert( _s[i] = new Selector<T,3>( _c[i], i, this, this->_scale.getScale(),
                                                   rad, selector_color ) );
        else
          this->insert( _s[i] = new Selector<T,3>( _c[i], i, this, rad, selector_color ) );
      _selectors       = true;
    }
    _selector_radius = rad;
    _selector_color  = selector_color;

    if( grid ) {
      if(!_sgv)
        _sgv = new SelectorGridVisualizer<T>;
      _sgv->setSelectors( _c, 0, _cl);
      _sgv->setColor( grid_color );
      SceneObject::insertVisualizer( _sgv );
      this->setEditDone();
    }
    _grid_color      = grid_color;
    _grid            = grid;
  }

  template <typename T>
  void BSpline<T>::hideSelectors()
  {
      // Remove Selectors
      if( _selectors ) {
          for( uint i = 0; i < _s.size(); i++ ) {
              this->remove( _s[i] );
              delete _s[i];
          }
          _s.clear();
          _selectors = false;
      }

      // Hide Selector Grid
      if(_sgv) {
        SceneObject::removeVisualizer( _sgv );
        _sgv->reset();
      }
  }

  template <typename T>
  void BSpline<T>::toggleSelectors()
  {
    if(_selectors)  hideSelectors();
    else            showSelectors(_selector_radius, _grid, _selector_color, _grid_color);
  }


  //*****************************************
  // Implementation of the formula for the curve
  //*****************************************
  template <typename T>
  void BSpline<T>::eval( T t, int d, bool /*1*/) const
  {
    this->_p.setDim( d + 1 );


    int closedFactor=_d; //Used to increase the _n when finding i for a closed curve
    if (!_cl)
      closedFactor=0;

    int i; //Would be updated in genBasis
    Vector<T, 3> b = Utilities::generateBasis(t, i, _t, _n+closedFactor, _d);

    int i2 = i;
    int i1 = i-1;

    if (_cl){
      //restarting i as 0 if equal to number of control points
      //increasing i to 1 if greater than number of control points
      if(i==_n)
        i2 = 0;
      else if (i>_n){
        i1 = 0;
        i2 = 1;
      }
    }

    this->_p[0] = b[0]*_c[i-2] + b[1]*_c[i1] + b[2]*_c[i2];
  }


  //*****************************************
  // Start and end of the defined domain (interval)
  //*****************************************
  template <typename T>
  T BSpline<T>::getStartP() const
  {
    return _t[_d];
  }

  template <typename T>
  T BSpline<T>::getEndP() const
  {
    int d = 0;
    if (_cl)
      d = _d; //_d = 2 remember
    return _t[_n + d];
  }


  //*****************************
  //Local Helper Functions
  //*****************************
  template<typename T>
  void BSpline<T>::localSimulate(double /*dt*/)
  {
    if (_updateSampleFlag){
      this->sample(100, 0);
      _updateSampleFlag = false;
    }

  }

#endif // GM_PARAMETRICS_CURVES_BSpline
} // END namespace GMlib
