
#include <iostream>

#include "scenario.h"
#include "testCurve.h"
#include "bSpline.h"
#include "blender.h"
#include "gerbs.h"
#include "gerbssurface.h"

#include "parametrics/curves/gmpbutterfly.h"
#include "parametrics/surfaces/gmpcylinder.h"

// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>
#include <parametrics/curves/gmpbeziercurve.h>

// qt
#include <QQuickItem>


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}




void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -10.0f, 10.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );


  /***************************************************************************
   *                                                                         *
   * Standar example, including path track and path track arrows             *
   *                                                                         *
   ***************************************************************************/

  GMlib::Material mm(GMlib::GMmaterial::polishedBronze());
  mm.set(45.0);
//BUTTERFLY
//  auto butterfly = new GMlib::PButterfly<float>;
//  butterfly->toggleDefaultVisualizer();
//  butterfly->sample(2000,1);
//  this->scene()->insert(butterfly);

////MY CURVE
  auto newCurve = new GMlib::TestCurve<float>();
  newCurve->toggleDefaultVisualizer();
  newCurve->sample(2000,0);
//  this->scene()->insert(newCurve);

////B SPLINE
//  //CONTROL POINTS
//  GMlib::DVector<GMlib::Vector<float, 3>> controlPoints(6);
//  controlPoints[0]= {0,0,0};
//  controlPoints[1]= {1,1,0};
//  controlPoints[2]= {2,0,0};
//  controlPoints[3]= {2,1,0};
//  controlPoints[4]= {1,2,0};
//  controlPoints[5]= {0,1,0};

//  auto newBSpline = new GMlib::BSpline<float>(controlPoints, true);
//  newBSpline->toggleDefaultVisualizer();
////  newBSpline->showSelectors();
//  newBSpline->sample(100,1);
//  this->scene()->insert(newBSpline);

////BLENDER
//  GMlib::DVector<GMlib::Vector<float, 3>> controlPoints(4);

//  controlPoints[0]= {0,0,0};
//  controlPoints[1]= {1,1,0};
//  controlPoints[2]= {0,2,0};
//  controlPoints[3]= {1,3,0};

//  auto newBSpline1 = new GMlib::BSpline<float>(controlPoints);
////  auto newBCurve1 = new GMlib::PBezierCurve<float>(controlPoints);
//  newBSpline1->toggleDefaultVisualizer();
//  newBSpline1->showSelectors();
//  newBSpline1->setColor(GMlib::GMcolor::yellow());
//  newBSpline1->sample(100,1);
//  this->scene()->insert(newBSpline1);

//  controlPoints[0]= {0,2,0};
//  controlPoints[1]= {1,3,0};
//  controlPoints[2]= {0,4,0};
//  controlPoints[3]= {1,5,0};

//  auto newBCurve2 = new GMlib::PBezierCurve<float>(controlPoints);
//  newBCurve2->toggleDefaultVisualizer();
//  newBCurve2->setColor(GMlib::GMcolor::pink());
//  newBCurve2->sample(100,1);
//  this->scene()->insert(newBCurve2);

//  auto blend = new GMlib::Blender<float>(newBSpline1,newBCurve2, 0.25f);
//  blend->toggleDefaultVisualizer();
//  blend->sample(100,0);
//  this->scene()->insert(blend);

//GERBS CURVE
  auto newGERB = new GMlib::GERBS<float>(newCurve,8,true);
  newGERB->toggleDefaultVisualizer();
  newGERB->sample(2000,0);
  newGERB->setColor(GMlib::GMcolor::blue());
//  this->scene()->insert(newGERB);

//GERBS SURFACE
  GMlib::PSurf<float,3>* cylinder = new GMlib::PCylinder<float>(1.5f, 1.5f, 5.0f);
  cylinder->toggleDefaultVisualizer();
  cylinder->rotateGlobal(M_PI/2,GMlib::Vector<float,3>(0,1,0));
  cylinder->sample(25,25,3,3);

  auto newGERBSSurface = new GMlib::GERBSSurf<float>(cylinder,10,10,GMlib::SurfaceShape::Cylinder);
  newGERBSSurface->toggleDefaultVisualizer();
  newGERBSSurface->sample(25,25,3,3);
//  this->scene()->insert(newGERBSSurface);

}

void Scenario::cleanupScenario() {

}

void Scenario::callDefferedGL() {

  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj[i]->replot();
}

