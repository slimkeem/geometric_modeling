#ifndef HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#include "parametrics/gmpcurve.h"

namespace GMlib {

  namespace Utilities {

    //*****************************************
    // Helper Classes
    //*****************************************
    template <typename T>
    void generateKnotVector(DVector<T> &_t, int _n, int _k, bool closed)
    {
      if(closed) {
        auto d = _k - 1;
        _t.setDim( _n + _k + d);

        for(int i = 0; i < _t.getDim(); i++ )
          _t[i] = T(i-d);
      }
      else {
        int dim = _n + _k;
        _t.setDim(dim);
        int step = dim - (2 * _k);
        int i = 0;
        for( ; i < _k; i++ )              // Set the start knots
          _t[i] = T(0);
        for( int j = 1; j <= step; j++ )  // Set the "step"-knots
          _t[i++] = T(j);
        for( ; i < dim; i++ )             // Set the end knots
          _t[i] = T(step+1);


      }
    }

    template <typename T>
    void generateGerbKnotVector(DVector<T> &_t, PCurve<T,3>* _curve, int _n, int _k, bool closed)
    {
      int dim = _n + _k;
      _t.setDim(dim);
      int step = dim - (2 * _k);
      auto delta = _curve->getParDelta()/(_n-1);
      int i = 0;
      for( ; i < _k; i++ )              // Set the start knots
        _t[i] = T(_curve->getParStart());
      for( int j = 1; j <= step; j++ )  // Set the "step"-knots
        _t[i++] = j*T(delta);
      for( ; i < dim; i++ )             // Set the end knots
        _t[i] = T(_curve->getParEnd());

      if(closed) {
        _t[0] = _t[1] - (_t[_n] - _t[_n-1]);
        _t[_n+1]    = _t[_n] + (_t[2] - _t[1]);
      }

      std::cout << _t << std::endl;
    }

    template<typename T>
    int findI(T t, DVector<T> _t, int _d, int _n)
    {
      for (int i = _d; i < _n; ++i) {
        if (_t[i] <= t && t < _t[i+1])
          return i;
      }
      return _n-1;
    }

    template<typename T>
    float calculateW(T t, DVector<T> _t, int d, int i)
    {
      return (t - _t[i]) / (_t[i+d] - _t[i]);
    }

    template<typename T>
    Vector<T,3> generateBasis(T t, int &i, DVector<T> _t, int _n, int _d)
    {
      i = findI(t, _t, _d, _n);

      float w1_i = calculateW(t,_t,1,i);
      float w2_iMinus = calculateW(t,_t,2,i-1);
      float w2_i = calculateW(t,_t,2,i);

      Vector<T, 3> b;
      DMatrix<T> A(1,2);
      DMatrix<T> B(2,3);

      A[0][0] = 1-w1_i;
      A[0][1] = w1_i;

      B[0][0] = 1-w2_iMinus;
      B[0][1] = w2_iMinus;
      B[0][2] = 0;

      B[1][0] = 0;
      B[1][1] = 1-w2_i;
      B[1][2] = w2_i;

      b[0] = A[0][0]*B[0][0];
      b[1] = A[0][0]*B[0][1] + A[0][1]*B[1][1];
      b[2] = A[0][1]*B[1][2];

      return b;
    }

    template<typename T>
    T calculateKnotDelta(DVector<T> _t, int _n, int _d, int _m)
    {
      return (_t[_n] - _t[_d])/(_m - 1);
    }

    template<typename T>
    DMatrix<T> generateAMatrix(DVector<T> _t, int _d, int _n, int _m)
    {
      DMatrix<T> A;
      A = GMlib::DMatrix<T>(_m, _n, T(0));

      const T dt = calculateKnotDelta(_t, _n, _d, _m);
      GMlib::Vector<T,3> basis;
      int i;
      for(int k =0; k < _m; k++)
      {
        auto t = _t[_d] + k*dt;
        basis = generateBasis(t, i, _t, _n, _d);
        for (int l = i-_d; l <= i; l++)
        {
          A[k][l] = basis[l-i+_d];
        }
      }

      return A;
    }

    template<typename T>
    void generateControlPoints(DMatrix<T> &_A, DVector<T> _t, DVector<Vector<T,3>> &_c,
                               DVector<Vector<T,3>> _point, int _d, int _n, int _m)
    {
      //!< Ax=b -> x=((A^t*A)^-1) *  (A^t*b)
      _A = generateAMatrix(_t, _d, _n, _m);

      auto A_transpose = _A.transpose();
      auto AtA = A_transpose * _A;
      auto b = A_transpose * _point;
      _c = AtA.invert()*b;
    }

    namespace BFunctions
    {
      template<typename T>
      T polynomialFunction(T t)
      {
        return ((3*t*t) - (2*t*t*t));
      }
    } // END namespace BFunctionsnamespace BFunctions

    namespace Animations {
      void expandCurve(double dt, double &x, double &y)
      {
          x += dt;
          y -= dt;
          std::cout << "Expanding: " << std::endl;

      }

      void shrinkCurve(double dt, double &x, double &y)
      {
          x -= dt;
          y += dt;
          std::cout << "Shrinking: " << std::endl;
      }
  } // END namespace Animations




  } // END namespace Utilities
} // END namespace GMlib
#endif // HELPERFUNCTIONS
