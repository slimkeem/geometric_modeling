#ifndef GM_PARAMETRICS_CURVES_TestCurve_H
#define GM_PARAMETRICS_CURVES_TestCurve_H


#include "parametrics/gmpcurve.h"

namespace GMlib {

template <typename T>
class TestCurve : public PCurve<T,3> {
  GM_SCENEOBJECT(TestCurve)
  public:
    TestCurve( T size = T(5) );
  TestCurve( const TestCurve<T>& copy );

  // Virtual public functions from PCurve
  bool          isClosed() const override;

protected:
  // Virtual protected functions from PCurve
  void          eval(T t, int d, bool l) const override;
  T             getStartP() const override;
  T             getEndP()   const override;
  // Intrinsic data for the curve
  T             _size;
}; // END class PSurface


//*****************************************
// Default Constructors
//*****************************************
template <typename T>
inline
TestCurve<T>::TestCurve( T size ) : PCurve<T,3>(20, 0, 2), _size(size) /*, _flaps(0.5)*/
{
  //  _sim_boundary = 0.7;
  //  _sim_speed    = 1;
}

//*****************************************
// Copy Constructors
//*****************************************
template <typename T>
inline
TestCurve<T>::TestCurve( const TestCurve<T>& copy ) : PCurve<T,3>(copy), _size(copy._size) {}

//*****************************************
// This Curve is Cyclical
//*****************************************
template <typename T>
bool TestCurve<T>::isClosed() const { return true;}

//*****************************************
// Implementation of the formula for the curve
//*****************************************
template <typename T>
void TestCurve<T>::eval( T t, int d, bool /*1*/) const{

  this->_p.setDim( d + 1 );
  const double ct   = cos(t);
  const double st   = sin(t);
  const double ct2  = cos(t/12);
  const double st2  = sin(t/12);
  const double a    = exp(ct) - 2*cos(4*t) - pow(st2, 5);
  const double a1   = -exp(ct)*st + 8*sin(4*t) - 5*pow(st2,4)*ct2/12;
  const double a2   = exp(ct)*st*st - exp(ct) * ct + 8*4*cos(4*t) - 5*(pow(st2,3)*pow(ct2,2)/3
                                                                       - pow(st2,5)/12)/12;

  //The position: c(t)
  this->_p[0][0] = _size * T(3*st + 2*sin(3*t));
  this->_p[0][1] = _size * T(ct - 2*cos(3*t));
  this->_p[0][2] =  /*fabs(this->_p[0][1])/2*/ cos(5*t);

  if (this->_dm == GM_DERIVATION_EXPLICIT) {
    if (d>0) {                            //1st Derivative
      this->_p[1][0]  = _size * T( -st*a + ct*a1 );
      this->_p[1][1]  = _size * T(  ct*a + st*a1 );
      this->_p[1][2]  =  fabs(this->_p[0][1])/2;
    }

    if (d>1) {                            //2nd Derivative
      this->_p[2][0]  = _size * T( -ct*a - st*a1 - st*a1 + ct*a2 );
      this->_p[2][1]  = _size * T( -st*a + ct*a1 + ct*a1 + st*a2 );
      this->_p[2][2]  =  fabs(this->_p[2][1])/2;    }
  }
}

//*****************************************
// Start and end of the defined domain (interval)
//*****************************************

template <typename T>
T TestCurve<T>::getStartP() const {
  return T(0);
}

template <typename T>
T TestCurve<T>::getEndP() const {
  return T( M_PI * 24.0 );
}

#endif // GM_PARAMETRICS_CURVES_TestCurve
} // END namespace GMlib
